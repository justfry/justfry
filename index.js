﻿const telegram = require("node-telegram-bot-api")
const pdfkit = require("pdfkit")
const fs = require("fs")
const path = require("path")
const shortid = require('shortid')
const dateformat = require('dateformat')
const sanitize = require('sanitize-filename')
const questions = require('./questions')
const pdfmake = require('pdfmake')
const nodemailer = require('nodemailer');
const aws = require('aws-sdk');
const timethat = require('timethat')
const Agent = require('socks5-https-client/lib/Agent')
let transporter = nodemailer.createTransport({
    type: 'smtp',
    host: process.env.SMTP_HOST,
    port: process.env.SMTP_PORT || 587,
    secure: false,
    tls: {rejectUnauthorized:false}
});

const token = "544165592:AAE2Uwll_Vlth-76F-9Le5TJOt7UzdmN_qo"
const password = "tD6PKEuC"
const fonts = {
    Roboto: {
		normal: 'fonts/Roboto-Regular.ttf',
		bold: 'fonts/Roboto-Medium.ttf',
		italics: 'fonts/Roboto-Italic.ttf',
		bolditalics: 'fonts/Roboto-MediumItalic.ttf'
    }
}
const pdfPrinter = new pdfmake(fonts)
//process.env.http_proxy="http://proxy:Fei2oe2picha@46.36.219.47:443";
//process.env.https_proxy="http://proxy:Fei2oe2picha@46.36.219.47:443";

var bot = new telegram(token, {
	polling: true,
	request:{
		agentClass: Agent,
		agentOptions: {
			socksHost:'46.36.219.47',
			socksPort:443,
			socksUsername:'proxy',
			socksPassword:'Fei2oe2picha',
		}
	}
})
var textToOut = []
let buttons = ["Фирменный стиль", "Управление репутацией", "Создание сайта", "Разработка Landing Page",
 "Разработка логотипа", "Контекстная реклама", "SMM-бриф"]

bot.on('message', (msg) => {
    if (!msg.text) bot.sendMessage(msg.from.id, "Пожалуйста, для ответов используйте только текст и кнопки") 
})

bot.on('text', function (msg){
    let i = getIndex(msg)
    if (buttons.includes(msg.text)) {
        textToOut[i]["breaf"] = msg.text
        textToOut[i]["isWork"] = true
        bot.sendMessage(msg.from.id, questions[0]["q"][textToOut[i]["question"]],{ reply_markup: JSON.stringify({
            hide_keyboard: true
        })})
    }
    else if (msg.text != "/start" && msg.text !="/admin" && msg.text != password && textToOut[i]["checkboxMod"]){
        textToOut[i]["checkbox"][textToOut[i]["checkboxIndex"]] += ' - ' + msg.text
        textToOut[i]["checkboxIndex"]++
        if (textToOut[i]["checkboxIndex"] != textToOut[i]["checkbox"].length){
            bot.sendMessage(msg.chat.id, textToOut[i]["checkbox"][textToOut[i]["checkboxIndex"]])
        }
        else{
            addAnswer(i, msg.chat.id, textToOut[i]["checkbox"])
            textToOut[i]["checkbox"] = []
            textToOut[i]["checkboxMod"] = false
        }
        
    }
    else if (msg.text != "/start" && msg.text !="/admin" && msg.text != password &&  textToOut[i]["edit"]){
        let j = buttons.indexOf(textToOut[i]["breaf"])
        if (textToOut[i]["indexEdit"] == -1){
                textToOut[i]["indexEdit"] = msg.text -1
                if (questions[j]["s"].includes(textToOut[i]["indexEdit"])){
                    keyboard = {
                        reply_markup: JSON.stringify({
                            inline_keyboard: 
                            
                                questions[j]["q"][textToOut[i]["indexEdit"]].split('\n').slice(1).map((element, index) => {
                                    return [{text: element.substring(1), callback_data: element.substring(1)}]
                                })
                            
                        })
                    }
                    bot.sendMessage(msg.chat.id, questions[j]["q"][textToOut[i]["indexEdit"]].split('\n')[0], keyboard)
                }
                else{
                    bot.sendMessage(msg.chat.id, questions[j]["q"][textToOut[i]["indexEdit"]], { reply_markup: JSON.stringify({
                    hide_keyboard: true
                    })})
                }
        }
        else {
            textToOut[i]["text"][textToOut[i]["indexEdit"]] = msg.text
            getKeyboard(msg, i, j)
            textToOut[i]["edit"] = false
            textToOut[i]["indexEdit"] = -1
        }
      
    }
    else if (msg.text != "/start" && msg.text !="/admin" && msg.text != password && textToOut[i]["isWork"]){
        textToOut[i]["checkbox"].length > 0 ?  addAnswer(i, msg.from.id, textToOut[i]["checkbox"] + '\n' + msg.text, msg) :  addAnswer(i, msg.from.id, msg.text, msg)
        textToOut[i]["checkbox"] = []
        }
    
})
bot.on('callback_query', (msg) => {
    bot.answerCallbackQuery(msg.id)
    let i = getIndex(msg.message)
    let j = buttons.indexOf(textToOut[i]["breaf"])
    if (msg.data == "end"){
        breaf = textToOut[i]["breaf"]
        companyName = textToOut[i]["text"][0].substring(0,18)
        endSubbmission(msg.message, breaf, companyName)
    }
    else
    if (msg.data == "edit"){
        textToOut[i]["edit"] = true
        textToOut[i]["indexEdit"] = -1
        bot.sendMessage(msg.message.chat.id, "Пожалуйста, введите номер вопроса:")
    }
    else
    if (textToOut[i]["indexEdit"] != -1 && msg.data != 'edit' && !msg.data.includes("◽")){
        if (!/Друг/.exec(msg.data) && !/уточнить/.exec(msg.data)) {
            if (textToOut[i]["checkbox"].length > 0){
            textToOut[i]["text"][textToOut[i]["indexEdit"]] = textToOut[i]["checkbox"]
            textToOut[i]["checkbox"] = []
            getKeyboard(msg.message, i, j)
            textToOut[i]["edit"] = false
            textToOut[i]["indexEdit"] = -1

            } 
            else{
                textToOut[i]["text"][textToOut[i]["indexEdit"]] = msg.data
                getKeyboard(msg.message, i, j)
                textToOut[i]["edit"] = false
                textToOut[i]["indexEdit"] = -1
            }
           
        } 
        else {
            bot.sendMessage(msg.message.chat.id, "Пожалуйста, уточните:")  
        } 
    }
    else
    if(textToOut[i]["indexEdit"] != -1 && msg.data != 'edit' && (msg.data.includes("◽"))){
         textToOut[i]["checkbox"].includes(msg.data.slice(1))?  textToOut[i]["checkbox"].splice(textToOut[i]["checkbox"].indexOf(msg.data.slice(1)), 1) : textToOut[i]["checkbox"].push(msg.data.replace(/◽/, "")) 
            
            keyboard = {
                reply_markup: JSON.stringify({
                    inline_keyboard: 
                        questions[j]["q"][textToOut[i]["indexEdit"]].split('\n').slice(1).map((element, index) => {
                            return textToOut[i]["checkbox"].includes(element.slice(2))?  [{text:  element.substring(1).replace(/◽/, "☑️"),  callback_data: element.substring(1)}]: [{text:  element.substring(1),  callback_data: element.substring(1)}]
                        })
                })
            }
            bot.editMessageReplyMarkup(keyboard.reply_markup, {chat_id: msg.message.chat.id, message_id: msg.message.message_id})
    }
    else
    if (msg.data.includes("◽")){
        textToOut[i]["checkbox"].includes(msg.data.slice(1))?  textToOut[i]["checkbox"].splice(textToOut[i]["checkbox"].indexOf(msg.data.slice(1)), 1) : textToOut[i]["checkbox"].push(msg.data.replace(/◽/, "")) 
            
            keyboard = {
                reply_markup: JSON.stringify({
                    inline_keyboard: 
                        questions[j]["q"][textToOut[i]["question"]].split('\n').slice(1).map((element, index) => {
                            return textToOut[i]["checkbox"].includes(element.slice(2))?  [{text:  element.substring(1).replace(/◽/, "☑️"),  callback_data: element.substring(1)}]: [{text:  element.substring(1),  callback_data: element.substring(1)}]
                        })
                })
            }
            bot.editMessageReplyMarkup(keyboard.reply_markup, {chat_id: msg.message.chat.id, message_id: msg.message.message_id})
    }
    else
    if (/Отправить/.exec(msg.data) && textToOut[i]["checkbox"].length > 0){
        if (/Укажите какие из компонентов должны присутствовать/.exec(msg.message.text)){
            textToOut[i]["checkboxMod"] = true
            textToOut[i]["checkboxIndex"] = 0
            bot.sendMessage(msg.message.chat.id, "Пожалуйста, дайте краткое описание")
            .then(()=>{
                bot.sendMessage(msg.message.chat.id, textToOut[i]["checkbox"][textToOut[i]["checkboxIndex"]])
            })
        }
        else
        if(textToOut[i]["checkbox"].includes('️Другое (указать что)') || textToOut[i]["checkbox"].includes('️Другое')){
            bot.sendMessage(msg.message.chat.id, "Пожалуйста, уточните:")
        }
        else {
            
            addAnswer(i, msg.message.chat.id, textToOut[i]["checkbox"])
            textToOut[i]["checkbox"] = []
        }
    }
    else {
         if (/Нет, не было опыта/.exec(msg.data)) {
          addAnswer(i, msg.message.chat.id, msg.data)
        }
         else
         !/Друг/.exec(msg.data) && !/уточнить/.exec(msg.data)  ? addAnswer(i, msg.message.chat.id, msg.data) : bot.sendMessage(msg.message.chat.id, "Пожалуйста, уточните:")
    }
})

bot.onText(/\/admin/, function(msg, match){
    let alreadyExists = false
    getIndex(msg) == -1? textToOut.push({"id": msg.chat.id, "text": [], "isWork": false, "edit": false, "indexEdit": -1, "companyName": "", "breaf": "", "question": 0, "admin": false, 
    "username": msg.from.username, "firstname": msg.from.first_name, "checkbox": [], "checkboxIndex": -1, "checkboxMod": false}) : alreadyExists = true
    if (alreadyExists){
        let i = getIndex(msg)
        textToOut[i]["text"] = []
        textToOut[i]["isWork"] = false
        textToOut[i]["breaf"] = ""
        textToOut[i]["question"] = 0
    }
    bot.sendMessage(msg.chat.id, "Введите пароль", { reply_markup: JSON.stringify({
        hide_keyboard: true
    })})
})
bot.onText(/tD6PKEuC/, function(msg, match){
    let i = getIndex(msg)
    bot.sendMessage(msg.chat.id, "Теперь вы получаете все заявки")
    textToOut[i]["admin"] = true;
})

bot.onText(/\/start/, function(msg, match){
    let alreadyExists = false
    getIndex(msg) == -1? textToOut.push({"id": msg.chat.id, "text": [], "isWork": false, "edit": false, "indexEdit": -1, "companyName": "", "breaf": "", "question": 0, "admin": false, 
    "username": msg.from.username, "firstname": msg.from.first_name, "checkbox": [], "checkboxIndex": -1, "checkboxMod": false, "timeStart": new Date(), "timeStand": 0}) : alreadyExists = true
    if (alreadyExists){
        let i = getIndex(msg)
        textToOut[i]["text"] = []
        textToOut[i]["isWork"] = false
        textToOut[i]["breaf"] = ""
        textToOut[i]["question"] = 0
        textToOut[i]["timeStart"] = new Date()
    }
    let text = "Выберете бриф"
    let keyboard = {
        reply_markup: JSON.stringify({
            keyboard: 
            [
                [buttons[0] , buttons[1]],
                [buttons[2] , buttons[3]],
                [buttons[4] , buttons[5]],
                [buttons[6]]
            ]
        })
    }
    bot.sendMessage(msg.chat.id, "Данные брифы составляются для правильного и своевременного выполнения исполнителем поставленной задачи. Бриф утверждается заказчиком для того, чтобы исполнитель ясно представлял задачи, стоящие перед ним. Бриф — это официальное задание исполнителю.   Вы можете не отвечать на часть вопросов, приведенных ниже, в этом случае исполнитель выполняет работу по своему усмотрению. Исполнитель принимает на себя обязательство о том, что коммерческая информация, полученная в рамках подготовки и реализации проекта, является конфиденциальной и не подлежит разглашению или передаче третьим лицам")
    .then(()=>{
        bot.sendMessage(msg.chat.id, 'Для пропуска вопроса, отпрвьте "-"' , keyboard)
    })
    .then(()=>{
        bot.sendMessage(msg.chat.id, text, keyboard)
    })
})

function endSubbmission(msg, breaf, companyName, userName){
    let i = getIndex(msg)
    timeEnd = new Date()
    timeStand = timethat.calc(textToOut[i]["timeStart"], timeEnd)
    textToOut[i]["timeStand"] = timeStand
    let fileName = getFileName(breaf, companyName, msg.from.username)
    makePDF(msg, fileName)
}
   
bot.on('polling_error', (error) => {
    console.log(error);  
  });

function makePDF(msg, fileName){
    let i = getIndex(msg)
    let j = buttons.indexOf(textToOut[i]["breaf"]) //индекс вопросв
    stream = fs.createWriteStream("./output/" + fileName)
    let heading = ""
    switch(textToOut[i]["breaf"]){
        case buttons[0]: heading = "Бриф на разработку фирменного стиля"; break
        case buttons[1]: heading = "Бриф на управление репутацией (SERM)"; break
        case buttons[2]: heading = "Бриф на разработку сайта"; break
        case buttons[3]: heading = "Бриф на создание Landing Page"; break
        case buttons[4]: heading = "Бриф на разработку логотипа"; break
        case buttons[5]: heading = "Бриф на планирование рекламной кампании"; break
        case buttons[6]: heading = "SMM-Бриф"; break
    }
    let docDefinition = {
        footer: function(currentPage, pageCount) { return {
            text : "СТРАНИЦА:    " + currentPage.toString(),
            alignment: 'right',
            fontSize: 6,
            margin: [0, 0, 60, 30],
            color: '#444'
            } 
        },
        content: [{
			image: 'img/logo.jpg',
			width: 200,
			alignment: 'center'
        },
        {
            text: heading + '\n', 
            fontSize: 18, 
            alignment: 'center',
            margin: [0, 30, 0, 30]
        },
        {
			text: [
				'Данный бриф составляется для правильного и своевременного выполнения исполнителем поставленной задачи. Бриф утверждается заказчиком для того, чтобы исполнитель ясно представлял задачи, стоящие перед ним. Бриф — это официальное задание исполнителю.   \n\n',
                'Вы можете не отвечать на часть вопросов, приведенных ниже, в этом случае исполнитель выполняет работу по своему усмотрению.\n\n ',
                'Исполнитель принимает на себя обязательство о том, что коммерческая информация, полученная в рамках подготовки и реализации проекта, является конфиденциальной и не подлежит разглашению или передаче третьим лицам.\n\n'
				],
            bold: false,
            fontSize: 10,
            margin: [45, 5, 0, 30]
		},
        {
            fontSize: 10,
            style: 'tableExample',
            table: {
                widths: [150,285],
				body: []
            }, fillColor: '#eeeeee', layout: {
                    hLineWidth: function (i, node) {
                        return 0.5;
                    },
                    vLineWidth: function (i, node) {
                        return  0.1;
                     },
        hLineColor: '#dbdbdb',
        vLineColor: '#dbdbdb',

        }
        }],
        styles: {
            tableExample: {
                margin: [45, 5, 0, 60]
            } 
        }
    }
    
 
    textToOut[i]["text"].forEach((el, index) => {
        //короче shortQuestion возвращает часть вопроса до \n можно через array.split('\n') но пока работает магия, это трогать я не буду
        shortQuestion = questions[j]["q"][index].substring(0, questions[j]["q"][index].indexOf("\n") == -1 ? questions[j]["q"][index].length : questions[j]["q"][index].indexOf("\n"))
        otherQuestion = questions[j]["q"][index].substring(questions[j]["q"][index].indexOf("\n") == -1 ? questions[j]["q"][index].length : questions[j]["q"][index].indexOf("\n"))
        docDefinition["content"][3]["table"]["body"].push([[{text:shortQuestion, bold: true}, otherQuestion.replace(/◽/gi, "•") + "\n"], textToOut[i]["text"][index]])
    })
          
    textToOut[i]["isWork"] = false
    textToOut[i]["text"] = []
    textToOut[i]["question"] = 0

 
    let pdfDoc = pdfPrinter.createPdfKitDocument(docDefinition);
    pdfDoc.pipe(stream)
    pdfDoc.end();

    stream.on('finish', () => {
        sendFile(msg, fileName)
    })
}
function sendFile(msg, fileName){
    let i = getIndex(msg)
    fs.createReadStream("./output/" + fileName) 
    bot.sendDocument(msg.chat.id,"./output/" + fileName)
    bot.sendMessage(msg.chat.id, "Спасибо за заполнение брифа! Мы свяжемся с вами в ближайшее время. \nНаш телефон: +7 (495) 215-54-99 \nКомпания Авентон")
    let userName = textToOut[i]["username"] ? textToOut[i]["username"] : ""
    transporter.sendMail({
        from: 'Aveton Telegram Bot<web@aventon.ru>',
        to: ['web@aventon.ru'],
        subject: 'Бриф от телеграм-бота',
        text: "Бриф заполнил: " + "@" + userName + "\nИмя: " + textToOut[i]["firstname"] + "\nНазвание компании: " + textToOut[i]["companyName"] + "\nБриф: " + textToOut[i]["breaf"] + "\nЗаполнение брифа заняло: " + textToOut[i]["timeStand"],
        ses: { // optional extra arguments for SendRawEmail
            Tags: [{
                Name: 'Бриф',
                Value: 'Бриф от телеграм-бота'
            }]
            
        },
        attachments: [
          {  filename: fileName,
            path:"./output/" + fileName
        }
        ]
    })

    textToOut.forEach((value, index) =>{
        if (textToOut[index]["admin"]) {
            let userName = textToOut[i]["username"] ? textToOut[i]["username"] : ""
            bot.sendMessage(textToOut[index]["id"], "Заявка от " + "@" + userName + "\nИмя: " + textToOut[i]["firstname"] + "\nНазвание компании: " + textToOut[i]["companyName"] + "\nБриф: " + textToOut[i]["breaf"] + "\nЗаполнение брифа заняло: " + textToOut[i]["timeStand"])
            bot.sendDocument(textToOut[index]["id"], "./output/" + fileName)
        }
    })
    
}
function getFileName(breaf, companyName, userName, date = new Date()){
    return sanitize(companyName + '_' + breaf + '_' + dateformat(date, 'dd.mm (HH-MM)') + '.pdf')
}
function getIndex(msg){
    for (let i = 0; i < textToOut.length; i++){
        if (msg.chat.id == textToOut[i]["id"]) return i
    }
    return -1
    
}
function getKeyboard(msg, i, j){
    var keyboardStr = JSON.stringify({
        inline_keyboard: [
          [
            {text:'Редактировать',callback_data:'edit'},
            {text:'Завершить',callback_data:'end'}
          ]
        ]
    });
    let fullView = ""
    textToOut[i]["text"].forEach((element, index) => {
        shortQuestion = questions[j]["q"][index].substring(0, questions[j]["q"][index].indexOf("\n") == -1 ? questions[j]["q"][index].length : questions[j]["q"][index].indexOf("\n"))
        fullView += "<strong>" + (index + 1) + ". " + shortQuestion + "</strong> "+ element  + "\n"
    });
    var keyboard = JSON.parse(keyboardStr)
    bot.sendMessage(msg.chat.id, fullView,{ parse_mode: "HTML", reply_markup: keyboard})   
}

function addAnswer (i, id, messageText, msg){
    let j = buttons.indexOf(textToOut[i]["breaf"])
            if (textToOut[i]["question"] == 0) textToOut[i]["companyName"] = messageText //Если первый вопрос, то это название компании
            
            textToOut[i]["text"].push(messageText)
            textToOut[i]["question"]++
            if (/Нет, не было опыта/.exec(messageText)){
                textToOut[i]["text"].push("-")
                textToOut[i]["question"]++
            }
            if (textToOut[i]["question"] == questions[j]["q"].length){
              getKeyboard(msg, i, j) 
              textToOut[i]["isWork"] = false
            }
            else{
                let keyboard = { reply_markup: JSON.stringify({
                    hide_keyboard: true
                })}
                
                if (questions[j]["s"].includes(textToOut[i]["question"])){
                    keyboard = {
                        reply_markup: JSON.stringify({
                            inline_keyboard: 
                            
                                questions[j]["q"][textToOut[i]["question"]].split('\n').slice(1).map((element, index) => {
                                    return [{text: element.substring(1), callback_data: element.substring(1)}]
                                })
                            
                        })
                    }
                    bot.sendMessage(id,"Вопрос " + (textToOut[i]["question"] + 1) + " из " + questions[j]["q"].length + "\n\n" +  questions[j]["q"][textToOut[i]["question"]].split('\n')[0], keyboard)
                }
                else{
                    bot.sendMessage(id,"Вопрос " + (textToOut[i]["question"] + 1) + " из " + questions[j]["q"].length + "\n\n" +  questions[j]["q"][textToOut[i]["question"]], { reply_markup: JSON.stringify({
                    hide_keyboard: true
                    })})
                }
            }
}

